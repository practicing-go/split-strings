package split-strings

func Solution(str string) []string {
  
  var result []string

  if len(str)%2 != 0 {
    str = str + "_"
  }
 

  i := 0
  for i < len(str){
    result = append(result, str[i : (i+2)])
    i = i+2
  }
 
  return result

}